import React from 'react';
import './LetterDisplay.less';

const LetterDisplay = (props) => {
  return (
    <p>
      {props.letter}
    </p>
  );
};

export default LetterDisplay;