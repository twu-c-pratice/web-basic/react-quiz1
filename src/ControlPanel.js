import React from 'react';
import LetterDisplay from './LetterDisplay';
import './ControlPanel.less';

class ControlPanel extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.listDisplays = this.listDisplays.bind(this);
  }

  listDisplays() {
    let gameAnswer = this.props.gameAnswer;
    return (
      gameAnswer.map((letter, index) =>
        <li key={index}>
          <LetterDisplay letter={letter}/>
        </li>
      )
    );
  }

  render() {
    return (
      <div>
        <h3>New Card</h3>
        <button onClick={this.props.newGame}>New Card</button>
        <ul>
          {this.listDisplays()}
        </ul>
        <button onClick={this.props.showAnswer}>Show Result</button>
      </div>
    );
  }
}

export default ControlPanel;