import React from 'react';
import GameResult from './GameResult';
import GameGuess from './GameGuess';

import './PlayingPanel.less';

class PlayingPanel extends React.Component {

  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <div>
        <GameResult/>
        <GameGuess/>
      </div>
    );
  }
}

export default PlayingPanel;