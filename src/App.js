import React from 'react';
import LetterGame from "./LetterGame";
import './App.less';

const App = () => {
  return (
    <LetterGame/>
  );
};

export default App;