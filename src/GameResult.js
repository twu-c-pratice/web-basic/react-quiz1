import React from 'react';

import './GameResult.less';

class GameResult extends React.Component {

  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <div>
        <h3>Your Result</h3>
        <ul></ul>
        <p>FAILED!</p>
      </div>
    );
  }
}

export default GameResult;
