import React from 'react';
import PlayingPanel from "./PlayingPanel";
import ControlPanel from './ControlPanel';
import './LetterDisplay.less';

class LetterGame extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      gameAnswer: [null, null, null, null],
      emptyAnswer: [null, null, null, null],
      hiddenAnswer: false,
      userAnswer: [null, null, null, null],
      answerCorrect: false
    };

    this.newGame = this.newGame.bind(this);
    this.generateFourLetters = this.generateFourLetters.bind(this);
    this.hiddenAnswer = this.hiddenAnswer.bind(this);
    this.showAnswer = this.showAnswer.bind(this);
  }

  generateFourLetters() {
    let result = [];
    let letterCharMax = 90;
    let letterCharMin = 65;
    for (let i = 0; i < 4; i++) {
      let random = Math.floor(Math.random() * (letterCharMax - letterCharMin + 1) + letterCharMin);
      result.push(String.fromCharCode(random).toUpperCase());
    }
    return result;
  }

  newGame() {
    this.showAnswer();
    this.setState({gameAnswer: this.generateFourLetters()});
    setTimeout(this.hiddenAnswer, 3000);
  }

  hiddenAnswer() {
    this.setState({hiddenAnswer: true});
  }

  showAnswer() {
    this.setState({hiddenAnswer: false});
  }

  updateUserAnswer(newUserAnswer) {
    this.setState({userAnswer: [...newUserAnswer]});
  }

  checkAnswer() {
    let userAnsWer = this.state.userAnswer;
    let answer = this.state.gameAnswer;
    if (answer.every((letter, index) => letter === userAnsWer[index])) {
      this.setState({answerCorrect: true});
    }
  }

  render() {
    const gameAnswer = this.state.hiddenAnswer ? this.state.emptyAnswer : this.state.gameAnswer;
    return (
      <div>
        <PlayingPanel answerCorrent={this.state.answerCorrect}/>
        <ControlPanel gameAnswer={gameAnswer} newGame={this.newGame}
                      showAnswer={this.showAnswer}/>
      </div>
    );
  }
}

export default LetterGame;