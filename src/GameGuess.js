import React from 'react';

import './GameGuess.less';

class GameGuess extends React.Component {

  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <div>
        <h3>Guess Card</h3>
        <input type="text"/>
        <button>Guess</button>
      </div>
    );
  }
}

export default GameGuess;
